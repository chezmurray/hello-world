# hello-world - create your first AppImage in 3 minutes or less

Download helloworld.zip and expand it.

To Compile the helloworld appimage and run it:

1 In a terminal: 

      appimagetool /pathto/helloworld helloworld.AppImage #compile
      chmod +x helloworld.AppImage                #make it executable
      ./helloworld.AppImage                       #run it in a terminal

2 You can edit main.sh with any bash code you like such as:
     audacious $@ and recompile it.

     
